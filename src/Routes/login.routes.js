const express = require('express');
const router = express.Router();
const LOGIN = require('../Controllers/login.controller')

module.exports = router;

router.post('/validate_access', LOGIN.validate_access)