const express = require('express');
const router = express.Router();
const CARS_SPECS = require('../Controllers/cars.controller')

module.exports = router;

router.post('/insert_car_specs', CARS_SPECS.insertCarSpect)

router.get('/get_all_cars_specs', CARS_SPECS.getAllCarsSpecs);

router.get('/get_all_types_of_measurement', CARS_SPECS.getTypesOfMeasurement);

router.post('/update_car_spec_by_id', CARS_SPECS.updateCarSpecById);

router.post('/insert_assigned_car_by_user', CARS_SPECS.insertAssignedCarByUser)

router.post('/get_all_assignet_cars_by_user', CARS_SPECS.getAllAssignetCarsByUser)

router.post('/remove_car_assignet', CARS_SPECS.removeCarAssignet)
