const authorize = require('./../Middlewares/autorization-middleware')

module.exports = (app) => {

    app.use('/api/users', authorize("custumer:Admin"),  require('./users.routes'))

    app.use('/api/cars', authorize("custumer:Admin"),  require('./cars.routes'))

    app.use('/api/maintenance_alert', authorize("custumer:Admin"),  require('./maintenance_alert.routes'))
    
    app.use('/api/spreadsheet', authorize("custumer:Admin"),  require('./spreadsheet.routes'))
    
    app.use('/api/login', require('./login.routes'))

};