const express = require('express');
const router = express.Router();
const USERS = require('../Controllers/users.controller')

module.exports = router;

router.post('/insert_new_user', USERS.insertNewUser);

router.post('/update_user', USERS.updateUserByID);

router.get('/get_all_users', USERS.getAllUsers);

router.get('/get_user_by_id/:id', USERS.getUserById);

// router.put('/update_user_by_id/:id', USERS.updateUserById);

// router.delete('/delete_user_by_id/:id', USERS.deleteUserById);
