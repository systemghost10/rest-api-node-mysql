const express = require('express');
const router = express.Router();
const SPREADSHEET = require('../Controllers/spreadsheet.controller')

module.exports = router;

//Hojas

router.post('/insert_spreadsheet_by_user', SPREADSHEET.insertSpreadSheetByUser)

router.post('/get_all_spreadsheet_by_id_user', SPREADSHEET.getAllSpreadSheetByIdUser)

// //Registros

router.post('/insert_spreadsheet_record_by_id', SPREADSHEET.insertSpreadRecordById)

router.post('/get_all_records_spredsheet_by_id', SPREADSHEET.getAllRecordsSpredsheetById)

