const express = require('express');
const router = express.Router();
const MAINTENANCE_ALERT = require('../Controllers/maintenance_alert.controller')

module.exports = router;

router.post('/insert_maintenance_alert', MAINTENANCE_ALERT.insertMaintenceAlert)

router.get('/get_all', MAINTENANCE_ALERT.getAllMaintenceAlerts)

router.get('/get_maintenance_alert_by/:id', MAINTENANCE_ALERT.getMaintenceAlertById)

router.post('/update_maintenance_alert_status', MAINTENANCE_ALERT.updateMaintenanceAlertStatus)