const SPREADSHEET_PROCESSOR = require('../Processors/spreadsheet.processor')

exports.insertSpreadSheetByUser = async (req, res) => {
    try{
        if(!req.body){
            res.status(400).send({
                message: "[Controller - insertSpreadSheetByUser]: No parameters detected "
            });
        }

        const params = {
            id:req.body.id,
            spreadsheet_name:req.body.spreadsheet_name,
            id_user:req.body.id_user,
            id_calculation_type:req.body.id_calculation_type
        }
        // console.log("SpreadSheetByUser params", params)
        SPREADSHEET_PROCESSOR.insertSpreadSheetByUser(params,(err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - insertSpreadSheetByUser]:", err});
            }
            else { 
                res.send(data);
                console.log('[Controller - insertSpreadSheetByUser]: Successful');
            }
        })
    }catch(err){
        console.log('=============================',err)
        res.status(500).send(
            { message:"[Controller - insertSpreadSheetByUser]:",error}
        );
    }

}

exports.insertSpreadRecordById = async (req, res) => {
    try{
        if(!req.body){
            res.status(400).send({
                message: "[Controller - insertSpreadRecordById]: No parameters detected "
            });
        }
        // var token
        
        const params = {
            id:req.body.id,
            id_leaf:req.body.id_leaf,
            id_user:req.body.id_user,
            price_per_liter:req.body.price_per_liter,
            total_liters:req.body.total_liters,
            odometro:req.body.odometro,
            id_store :req.body.id_store,
            id_payment:req.body.id_payment,
            tank:req.body.tank,
            real_distance:req.body.real_distance,
            kpl:req.body.kpl,
            mpg:req.body.mpg,
            kilometer_per_liter:req.body.kilometer_per_liter,
            performance:req.body.performance,
            id_performance_status:req.body.id_performance_status,
            days:req.body.days,
            total_spent:req.body.total_spent,
            calculation_type:req.body.calculation_type,
        }
        console.log("users params", params)
        SPREADSHEET_PROCESSOR.insertSpreadRecordById(params,(err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - insertSpreadRecordById]:", err});
            }
            else { 
                res.send(data);
                console.log('[Controller - insertSpreadRecordById]: Successful');
            }
        })
    }catch(err){
        console.log('=============================',err)
        res.status(500).send(
            { message:"[Controller - insertSpreadRecordById]:",error}
        );
    }

}

exports.getAllSpreadSheetByIdUser = async (req, res) => {
    const params = {
        id_user:req.body.id_user
    }
    console.log('parametros controllador:', params)
    try{
        SPREADSHEET_PROCESSOR.getAllSpreadSheetByIdUser(params,(err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - getAllSpreadSheetByIdUser]:", err});
            }
            else { 
                console.log('[getAllSpreadSheetByIdUser controller]:', data)
                res.send(data);
                console.log('[Controller - getAllSpreadSheetByIdUser]: Successful');
            }
        })
    }catch(err){
        res.status(500).send({
            message: error.message || "some rong"
        })
    }

}

exports.getAllRecordsSpredsheetById = async (req, res) => {
    try{
        if(!req.body){
            res.status(400).send({
                message: "[Controller - getAllRecordsSpredsheetById]: No parameters detected "
            });
        }

        const params = {
            id_user:req.body.id_user,
            id_leaf:req.body.id_leaf
        }
        console.log('parametros controllador:', params)

        
        SPREADSHEET_PROCESSOR.getAllRecordsSpredsheetById(params,(err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - getAllRecordsSpredsheetById]:", err});
            }
            else { 
                console.log('[getAllRecordsSpredsheetById controller]:', data)
                res.send(data);
                console.log('[Controller - getAllRecordsSpredsheetById]: Successful');
            }
        })
    }catch(err){
        res.status(500).send({
            message: error.message || "some rong"
        })
    }

}

// exports.updateSpreadSheetById = async (req, res) => {
//     try{
//         if(!req.body){
//             res.status(400).send({
//                 message: "[Controller - updateSpreadSheetById]: No parameters detected "
//             });
//         }

//         const params = {
//             id:req.body.id,
//             spreadsheet_name:req.body.spreadsheet_name,
//             id_user:req.body.id_user,
//             id_calculation_type:req.body.id_calculation_type,
//         }
//         // console.log("SpreadSheetByUser params", params)
//         SPREADSHEET_PROCESSOR.updateSpreadSheetById(params,(err,data) =>{
//             if(err){
//                 res.status(500).send({
//                 message: "[Controller - updateSpreadSheetById]:", err});
//             }
//             else { 
//                 res.send(data);
//                 console.log('[Controller - updateSpreadSheetById]: Successful');
//             }
//         })
//     }catch(err){
//         console.log('=============================',err)
//         res.status(500).send(
//             { message:"[Controller - updateSpreadSheetById]:",error}
//         );
//     }

// }

// exports.updateSpreadSheetById = async (req, res) => {
//     try{
//         if(!req.body){
//             res.status(400).send({
//                 message: "[Controller - updateSpreadSheetById]: No parameters detected "
//             });
//         }        
//         const params = {
//             id:req.body.id,
//             user_name:req.body.user_name,
//             password:req.body.password,
//             email:req.body.email,
//             phone:req.body.phone,
//             active:req.body.active,
//         }
//         console.log("PARAMS - updateSpreadSheetById", params)
//         SPREADSHEET_PROCESSOR.updateSpreadSheetById(params,(err,data) =>{
//             if(err){
//                 res.status(500).send({
//                 message: "[Controller - updateSpreadSheetById]:", err});
//             }
//             else { 
//                 res.send(data);
//                 console.log('[Controller - updateSpreadSheetById]: Successful');
//             }
//         })
//     }catch(err){
//         res.status(500).send(
//             { message:"[Controller - updateSpreadSheetById]:",error}
//         );
//     }

// }

// exports.updateRecordSpredSheetById = async (req, res) => {
//     try{
//         if(!req.body){
//             res.status(400).send({
//                 message: "[Controller - updateRecordSpredSheetById]: No parameters detected "
//             });
//         }        
//         const params = {
//             id:req.body.id,
//             user_name:req.body.user_name,
//             password:req.body.password,
//             email:req.body.email,
//             phone:req.body.phone,
//             active:req.body.active,
//         }
//         console.log("PARAMS - updateRecordSpredSheetById", params)
//         SPREADSHEET_PROCESSOR.updateRecordSpredSheetById(params,(err,data) =>{
//             if(err){
//                 res.status(500).send({
//                 message: "[Controller - updateRecordSpredSheetById]:", err});
//             }
//             else { 
//                 res.send(data);
//                 console.log('[Controller - updateRecordSpredSheetById]: Successful');
//             }
//         })
//     }catch(err){
//         res.status(500).send(
//             { message:"[Controller - updateRecordSpredSheetById]:",error}
//         );
//     }

// }