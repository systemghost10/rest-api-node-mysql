const CARS_SPECS = require('../Processors/cars.processor')

exports.insertCarSpect = async (req, res) => {
    try{

        if(!req.body){
            res.status(400).send({
                message: "[Controller - insertCarSpect]: No parameters detected "
            });
        }

        const params = {
            id:req.body.id,
            car_name:req.body.car_name,
            tank:req.body.tank,
            motor_name:req.body.motor_name,
            kpl:req.body.kpl,
            mpg:req.body.mpg,
            id_nationality:req.body.id_nationality,
            active:req.body.active,
        }

        CARS_SPECS.insertCarSpect(params,(err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - insertCarSpect]:", err});
                res.send(err);
            }
            else { 
                res.send(data);
                console.log('[Controller - insertCarSpect]: Successful');
            }
        })

    }catch(err){
        res.status(500).send(
            { message:"[Controller - insertCarSpect]:",error}
        );
    }

}

exports.insertAssignedCarByUser = async (req, res) => {
    try{

        if(!req.body){
            res.status(400).send({
                message: "[Controller - insertAssignedCarByUser]: No parameters detected "
            });
        }

        const params = {
            id:req.body.id,
            id_car:req.body.id_car,
            id_user:req.body.id_user,
            active:req.body.active,
        }

        CARS_SPECS.insertAssignedCarByUser(params,(err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - insertAssignedCarByUser]:", err});
                res.send(err);
            }
            else { 
                res.send(data);
                console.log('[Controller - insertAssignedCarByUser]: Successful');
            }
        })

    }catch(err){
        res.status(500).send(
            { message:"[Controller - insertAssignedCarByUser]:",error}
        );
    }

}

exports.getAllCarsSpecs = async (req, res) => {
    try{
        CARS_SPECS.getAllCarsSpecs((err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - getAllCarsSpecs]:", err});
            }
            else { 
                console.log('[getAllCarsSpecs controller]:', data)
                res.send(data);
                console.log('[Controller - getAllCarsSpecs]: Successful');
            }
        })
    }catch(err){
        res.status(500).send({
            message: error.message || "some rong"
        })
    }

}

exports.getCarSepcById = async (req, res) => {
    try{
        if(!req.body){
            res.status(400).send({
                message: "[Controller - getCarSepcById]: No parameters detected "
            });
        }

        const params = {
            id:req.body.id
        }
        console.log('parametros controllador:', params)

        
        CARS_SPECS.getCarSepcById(params,(err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - getCarSepcById]:", err});
            }
            else { 
                console.log('[getCarSepcById controller]:', data)
                res.send(data);
                console.log('[Controller - getCarSepcById]: Successful');
            }
        })
    }catch(err){
        res.status(500).send({
            message: error.message || "some rong"
        })
    }

}

exports.getAllAssignetCarsByUser = async (req, res) => {
    try{
        if(!req.body){
            res.status(400).send({
                message: "[Controller - getAllAssignetCarsByUser]: No parameters detected "
            });
        }

        const params = {
            id_user:req.body.id_user
        }
        console.log('parametros controllador:', params)

        
        CARS_SPECS.getAllAssignetCarsByUser(params,(err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - getAllAssignetCarsByUser]:", err});
            }
            else { 
                console.log('[getAllAssignetCarsByUser controller]:', data)
                res.send(data);
                console.log('[Controller - getAllAssignetCarsByUser]: Successful');
            }
        })
    }catch(err){
        res.status(500).send({
            message: error.message || "some rong"
        })
    }
}

exports.removeCarAssignet = async (req, res) => {
    try{
        if(!req.body){
            res.status(400).send({
                message: "[Controller - removeCarAssignet]: No parameters detected "
            });
        }
        
        const params = {
            id:req.body.id,
            id_user:req.body.id_user
        }
        console.log('parametros controllador:', params)
        
        CARS_SPECS.removeCarAssignet(params,(err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - removeCarAssignet]:", err});
            }
            else { 
                console.log('[removeCarAssignet controller]:', data)
                res.send(data);
                console.log('[Controller - removeCarAssignet]: Successful');
            }
        })
    }catch(err){
        res.status(500).send({
            message: error.message || "some rong"
        })
    }

}

exports.updateCarSpecById = async (req, res) => {
    try{
        if(!req.body){
            res.status(400).send({
                message: "[Controller - updateCarSpecById]: No parameters detected "
            });
        }        
        const params = {
            id:req.body.id,
            car_name:req.body.car_name,
            tank:req.body.tank,
            motor_name:req.body.motor_name,
            kpl:req.body.kpl,
            mpg:req.body.mpg,
            id_nationality:req.body.id_nationality,
            active:req.body.active,
        }
        // console.log("PARAMS - updateCarSpecById", params)
        CARS_SPECS.updateCarSpecById(params,(err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - updateCarSpecById]:", err});
            }
            else { 
                res.send(data);
                console.log('[Controller - updateCarSpecById]: Successful');
            }
        })
    }catch(err){
        res.status(500).send(
            { message:"[Controller - updateCarSpecById]:",error}
        );
    }

}

exports.getTypesOfMeasurement = async (req, res) => {
    try{
     
        CARS_SPECS.getTypesOfMeasurement((err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - getTypesOfMeasurement]:", err});
            }
            else { 
                console.log('[getTypesOfMeasurement controller]:', data)
                res.send(data);
                console.log('[Controller - getTypesOfMeasurement]: Successful');
            }
        })
    }catch(err){
        res.status(500).send({
            message: error.message || "some rong"
        })
    }

}


