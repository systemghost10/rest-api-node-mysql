const MAINTENANCE_ALERT = require('../Processors/maintenance_alert.processor')

exports.insertMaintenceAlert = async (req, res) => {
    try{

        if(!req.body){
            res.status(400).send({
                message: "[Controller - insertMaintenceAlert]: No parameters detected "
            });
        }

        const params = {
            id:req.body.id,
            id_user:req.body.id_user,
            mechanical_motor:req.body.mechanical_motor,
            reactor:req.body.reactor,
            mechanical_motor_value:req.body.mechanical_motor_value,
            reactor_value:req.body.reactor_value,
            id_status:req.body.reactor_value
        }

        MAINTENANCE_ALERT.insertMaintenanceAlert(params,(err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - insertMaintenceAlert]:", err});
                res.send(err);
            }
            else { 
                res.send(data);
                console.log('[Controller - insertMaintenceAlert]: Successful');
            }
        })

    }catch(err){
        res.status(500).send(
            { message:"[Controller - insertMaintenceAlert]:",error}
        );
    }
}

exports.getAllMaintenceAlerts = async (req, res) => {
    try{
        MAINTENANCE_ALERT.getAllMaintenceAlerts((err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - getAllTypes]:", err});
            }
            else { 
                console.log('[getAllMaintenceAlerts controller]:', data)
                res.send(data);
                console.log('[Controller - getAllMaintenceAlerts]: Successful');
            }
        })
    }catch(err){
        res.status(500).send({
            message: error.message || "some rong"
        })
    }

}

exports.getMaintenceAlertById = async (req, res) => {
    try{
        if(!req.body){
            res.status(400).send({
                message: "[Controller - getMaintenceAlertById]: No parameters detected "
            });
        }

        const params = {
            id:req.params.id
        }
        console.log('parametros controllador:', params)

        
        MAINTENANCE_ALERT.getMaintenceAlertById(params,(err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - getMaintenceAlertById]:", err});
            }
            else { 
                console.log('[getMaintenceAlertById controller]:', data)
                res.send(data);
                console.log('[Controller - getMaintenceAlertById]: Successful');
            }
        })
    }catch(err){
        res.status(500).send({
            message: error.message || "some rong"
        })
    }

}

exports.updateMaintenanceAlertStatus = async (req, res) => {
    try{
        if(!req.body){
            res.status(400).send({
                message: "[Controller - updateMaintenanceAlertStatus]: No parameters detected "
            });
        }        
        const params = {
            id:req.body.id,
            id_status:req.body.id_status
        }
        console.log("PARAMS - updateMaintenanceAlertStatus", params)
        MAINTENANCE_ALERT.updateMaintenanceAlertStatus(params,(err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - updateMaintenanceAlertStatus]:", err});
            }
            else { 
                res.send(data);
                console.log('[Controller - updateMaintenanceAlertStatus]: Successful');
            }
        })
    }catch(err){
        res.status(500).send(
            { message:"[Controller - updateMaintenanceAlertStatus]:",error}
        );
    }

}
