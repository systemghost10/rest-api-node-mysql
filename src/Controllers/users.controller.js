const USERS_PROCESSOR = require('../Processors/users.processor')

exports.insertNewUser = async (req, res) => {
    try{
        if(!req.body){
            res.status(400).send({
                message: "[Controller - insertNewUser]: No parameters detected "
            });
        }
        // var token
        
        const params = {
            id:req.body.id,
            user_name:req.body.user_name,
            password:req.body.password,
            user_rol:req.body.user_rol,
            email:req.body.email,
            phone:req.body.phone,
            name:req.body.name,
            active:req.body.active,
        }
        console.log("users params", params)
        // token = await generate_token.createToken(params)
        // console.log('TOKEN GENERADO DESDE EL USER:', token)
        // res.send(token);
        USERS_PROCESSOR.insertNewUser(params,(err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - insertNewUser]:", err});
            }
            else { 
                res.send(data);
                console.log('[Controller - insertNewUser]: Successful');
            }
        })
    }catch(err){
        console.log('=============================',err)
        res.status(500).send(
            { message:"[Controller - insertNewUser]:",error}
        );
    }

}

exports.getAllUsers = async (req, res) => {
    try{
        USERS_PROCESSOR.getAllUsers((err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - getAllTypes]:", err});
            }
            else { 
                console.log('[getAllUsers controller]:', data)
                res.send(data);
                console.log('[Controller - getAllUsers]: Successful');
            }
        })
    }catch(err){
        res.status(500).send({
            message: error.message || "some rong"
        })
    }

}

exports.getUserById = async (req, res) => {
    try{
        if(!req.body){
            res.status(400).send({
                message: "[Controller - getUserById]: No parameters detected "
            });
        }

        const params = {
            id:req.params.id
        }
        console.log('parametros controllador:', params)

        
        USERS_PROCESSOR.getUserById(params,(err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - getUserById]:", err});
            }
            else { 
                console.log('[getUserById controller]:', data)
                res.send(data);
                console.log('[Controller - getUserById]: Successful');
            }
        })
    }catch(err){
        res.status(500).send({
            message: error.message || "some rong"
        })
    }

}

// exports.updateUserById = async (req, res) => {
//     try{
//         if(!req.body){
//             res.status(400).send({
//                 message: "[Controller - updateUserById]: No parameters detected "
//             });
//         }

//         const params = {
//             id:req.body.id,
//             name:req.body.name,
//             password:req.body.password
//         }

        
//         USERS_PROCESSOR.updateUserById(params,(err,data) =>{
//             if(err){
//                 res.status(500).send({
//                 message: "[Controller - updateUserById]:", err});
//             }
//             else { 
//                 res.send(data);
//                 console.log('[Controller - updateUserById]: Successful');
//             }
//         })
//     }catch(err){
//         res.status(500).send(
//             { message:"[Controller - updateUserById]:",error}
//         );
//     }

// }

exports.deleteUserById = async (req, res) => {
    try{
        if(!req.body){
            res.status(400).send({
                message: "[Controller - deleteUserById]: No parameters detected "
            });
        }
        const params = {
            id:req.params,
        }
        USERS_PROCESSOR.deleteUserById(params,(err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - deleteUserById]:", err});
            }
            else { 
                res.send(data);
                console.log('[Controller - deleteUserById]: Successful');
            }
        })
    }catch(err){
        res.status(500).send(
            { message:"[Controller - deleteUserById]:",error}
        );
    }

}

exports.updateUserByID = async (req, res) => {
    try{
        if(!req.body){
            res.status(400).send({
                message: "[Controller - updateUserByID]: No parameters detected "
            });
        }        
        const params = {
            id:req.body.id,
            user_name:req.body.user_name,
            password:req.body.password,
            email:req.body.email,
            phone:req.body.phone,
            active:req.body.active,
        }
        console.log("PARAMS - updateUserByID", params)
        USERS_PROCESSOR.updateUserByID(params,(err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - updateUserByID]:", err});
            }
            else { 
                res.send(data);
                console.log('[Controller - updateUserByID]: Successful');
            }
        })
    }catch(err){
        res.status(500).send(
            { message:"[Controller - updateUserByID]:",error}
        );
    }

}
 