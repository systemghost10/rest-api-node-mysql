const LOGIN = require('../Processors/login.processor')
///token alv
const jwt = require('jsonwebtoken');
const config = require('./../Middlewares/config');

exports.validate_access = async (req, res) => {
    try{
        if(!req.body){
            res.status(400).send({
                message: "[Controller - validate_access]: No parameters detected "
            });
        }
        
        
        const params = {
            user_name:req.body.user_name,
            password:req.body.password,
        }
        console.log("Controller validate_access:", params)
        LOGIN.validate_access(params,(err,data) =>{
            if(err){
                res.status(500).send({
                message: "[Controller - validate_access]:", err});
            }
            else { 
                if(data === [] || data.length === 0 ){
                    res.send({"status":"error", "message":"Usuario/Contraseña Incorrecta"});
                    return
                }else{
                    var json =  JSON.parse(data);
                    // console.log('>> json: ', json);
                    // console.log('>> user.name: ', json[0].user_name);
                    var token
                    const payload = {
                        id:json[0].id,
                        user_name: json[0].user_name,
                        password: json[0].password,
                        name: json[0].name,
                        scopes: [`custumer:${json[0].user_rol}`]
                    };
                    token = jwt.sign(payload, config.JWT_SECRET);
                    // console.log('token',token)
                    res.send({"status":"ok", "token":token});
                    // res.send(data);
                }
                console.log('[Controller - validate_access]: Successful');
            }
        })
        
    }catch(err){
        console.log('=============================',err)
        res.status(500).send(
            { message:"[Controller - validate_access]:",error}
        );
    }

}