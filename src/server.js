const express = require('express');
const cors = require('cors')
const app = express();


// Parse application/x-www-form-urlencoded

app.use(cors());
app.use(express.urlencoded({ extended: false }));

// Hearders to resive data from front-end
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization'
    );
    next();
});

app.options('*', cors());
app.use(cors());
// app.use(bodyParser.json())

// Parse application/json
app.use(express.json());

// app.options('*', cors())

// Routes 
require('./Routes')(app);

// Starting the server
app.set('port', process.env.PORT || 3000);

app.listen(app.get('port'), () =>{
    console.log('Server on port', app.get('port'))
})