const mysqlConection = require('../Config/database')

exports.insertCarSpect = (params,result) => {
    const {id, car_name, tank, motor_name,kpl,mpg,id_nationality,active} = params
    console.log('insertCarSpect - processor',params)
    const query = `
    CALL insert_car_spect(?,?,?,?,?,?,?,?);`;
    mysqlConection.query(query, 
        [id, 
        car_name, 
        tank,
        motor_name,
        kpl,
        mpg,
        id_nationality,
        active], 
        (err, rows, fields) => {
        if(!err){
            result(null, ({status:'Car Spect created'}))
        }else{
            console.log(err);
        }
    });
}; 

exports.insertAssignedCarByUser = (params,result) => {
    const {id, id_car, id_user, active} = params
    console.log('insertAssignedCarByUser - processor',params)
    const query = `
    CALL insert_assign_car(?,?,?,?);`;
    mysqlConection.query(query, 
        [id, 
        id_car, 
        id_user,
        active], 
        (err, rows, fields) => {
        if(!err){
            result(null, ({status:'Car Assigned'}))
        }else{
            console.log(err);
        }
    });
}; 

///Tipo de calculo por nacionalidad de carro
exports.getTypesOfMeasurement = (result) => {
    mysqlConection.query('CALL get_all_calculation_of_measurement', (err, rows, fields) => {
        if(!err){
            console.log('processor: ok')           
            result(null,rows[0])
        }else{
            throw err
        }
    });   
}

exports.getAllCarsSpecs = (result) => {
    mysqlConection.query('CALL get_all_cars_specs', (err, rows, fields) => {
        if(!err){
            console.log('processor: ok')           
            result(null,rows[0])
        }else{
            throw err
        }
    });   
}

///Este no se utiliza
exports.getCarSepcById = (params,result) => {
    console.log('id:',params.id)
    const {id} = params;
    mysqlConection.query('SELECT * from cars_specs WHERE id = ?', [id], 
    (err, rows, fields) =>{
        if(!err){
            console.log('data map', rows)
            result(null,rows)
        }else{
            console.log(err);
        }
    })
};   

exports.getAllAssignetCarsByUser = (params,result) => {
    console.log('id_user:',params.id_user)
    const {id_user} = params;
    const query = `
    CALL get_assigned_cars_by_user(?);`;
    mysqlConection.query(query, [id_user], 
    (err, rows, fields) =>{
        if(!err){
            result(null,rows[0])
        }else{
            console.log(err);
        }
    })
};   


exports.removeCarAssignet = (params,result) => {
    console.log('id_user:',params)
    const {id, id_user} = params;
    let query = `
    CALL desactivate_card_assignet(?,?)`;
    mysqlConection.query(query,[id,id_user], 
    (err, rows, fields) =>{
        if(!err){
            result(null,{'removeCarAssignet':'ok'})
        }else{
            console.log(err);
        }
    })
};   


exports.updateCarSpecById = (params,result) => {
    const {id, car_name, tank, motor_name, kpl, mpg,id_nationality,active} = params
    // console.log('updateCarSpecById - processor',params)
    const query = `
    CALL update_car_spect_by_id(?,?,?,?,?,?,?,?);`;
    mysqlConection.query(query, [id, car_name, tank, motor_name, kpl, mpg,id_nationality,active], (err, rows, fields) => {
        if(!err){
            result(null, ({status:'Car Spec Update'}))
        }else{
            console.log(err);
        }
    });
}; 

