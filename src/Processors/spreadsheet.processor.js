const mysqlConection = require('../Config/database')


exports.insertSpreadSheetByUser = (params,result) => {
    const {id, id_user, id_calculation_type, spreadsheet_name} = params
    // console.log('insertSpreadSheetByUser - processor',params)
    const query = `
    CALL insert_spreadsheet(?,?,?,?);
    `;
    mysqlConection.query(query, 
        [id, 
        id_user,
        id_calculation_type,
        spreadsheet_name
    ], (err, rows, fields) => {
        if(!err){;
            result(null, ({status:'spreadsheet name created'}))
        }else{
            console.log(err);
        }
    });
}; 

exports.insertSpreadRecordById = (params,result) => {
    const {
        id,
        id_leaf,
        id_user,
        price_per_liter,
        total_liters,
        odometro,
        id_store,
        id_payment,
        tank,
        real_distance,
        kpl,
        mpg,
        kilometer_per_liter,
        performance,
        id_performance_status,
        days,
        total_spent,
        calculation_type,
    } = params
    console.log('insertSpreadRecordById - processor',params)
    const query = `
    CALL insert_spreadsheet_record(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);
    `;
    mysqlConection.query(query, 
        [
        id,
        id_leaf,
        id_user,
        price_per_liter,
        total_liters,
        odometro,
        id_store,
        id_payment,
        tank,
        real_distance,
        kpl,
        mpg,
        kilometer_per_liter,
        performance,
        id_performance_status,
        days,
        total_spent,
        calculation_type], (err, rows, fields) => {
        if(!err){
            // res.json();
            result(null, ({status:'User created'}))
        }else{
            console.log(err);
        }
    });
}; 

exports.getAllSpreadSheetByIdUser = (params,result) => {
    console.log('params:',params)
    const {id_user, id_car} = params;
    const query = `
    CALL get_all_spread_sheet_by_id_user(?,?);`;
    mysqlConection.query(query, [id_user, id_car], (err,
    rows, fields) =>{
        if(!err){
            result(null,rows)
        }else{
            console.log(err);
        }
    })
};   

exports.getAllRecordsSpredsheetById = (params,result) => {
    console.log('params:',params)
    const {id_user, id_leaf} = params;
    mysqlConection.query('SELECT * from spreadsheet_records WHERE id_user = ? AND id_leaf = ?', [id_user, id_leaf], (err,
    rows, fields) =>{
        if(!err){
            result(null,rows)
        }else{
            console.log(err);
        }
    })
};  



// exports.updateSpreadSheetBy Id = (params,result) => {
//     const {id, user_name, password, email, phone, active} = params
//     const query = `
//     CALL update_user(?,?,?,?,?,?);`;
//     mysqlConection.query(query, [id, user_name, password, email, phone, active], (err, rows, fields) => {
//         if(!err){
//             result(null, ({status:'User Update'}))
//         }else{
//             console.log(err);
//         }
//     });
// }; 

// exports.updateRecordSpredSheetById = (params,result) => {
//     const {id, user_name, password, email, phone, active} = params
//     const query = `
//     CALL update_user(?,?,?,?,?,?);`;
//     mysqlConection.query(query, [id, user_name, password, email, phone, active], (err, rows, fields) => {
//         if(!err){
//             result(null, ({status:'User Update'}))
//         }else{
//             console.log(err);
//         }
//     });
// }; 

