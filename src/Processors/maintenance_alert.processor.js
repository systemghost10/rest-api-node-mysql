const mysqlConection = require('../Config/database')


exports.insertMaintenanceAlert = (params,result) => {
    const {id, id_user, mechanical_motor, reactor,mechanical_motor_value,reactor_value,id_status} = params
    console.log('insertMaintenceAlert - processor',params)
    const query = `
    CALL insert_new_maintenance_alert(?,?,?,?,?,?,?);`;
    mysqlConection.query(query, 
        [id, 
        id_user, 
        mechanical_motor,
        reactor,
        mechanical_motor_value,
        reactor_value,
        id_status], 
        (err, rows, fields) => {
        if(!err){
            result(null, ({status:'Car Spect created'}))
        }else{
            console.log(err);
        }
    });
}; 


exports.getAllMaintenceAlerts = (result) => {
    mysqlConection.query('SELECT * FROM maintenance_alert', (err, rows, fields) => {
        console.log('err'.err)
        if(!err){
            console.log('processor: ok')           
            result(null,rows)
        }else{
            throw err
        }
    });   
}

exports.getMaintenceAlertById = (params,result) => {
    console.log('id:',params.id)
    const {id} = params;
    mysqlConection.query('SELECT * from maintenance_alert WHERE id = ?', [id], (err,
    rows, fields) =>{
        if(!err){
            result(null,rows)
        }else{
            console.log(err);
        }
    })
};   


exports.updateMaintenanceAlertStatus = (params,result) => {
    const {id, id_status} = params
    const query = `
    CALL update_maintenance_alert_status(?,?);`;
    mysqlConection.query(query, [id, id_status], (err, rows, fields) => {
        if(!err){
            result(null, ({status:'status Update'}))
        }else{
            console.log(err);
        }
    });
}; 