const mysqlConection = require('../Config/database')


exports.validate_access = async (params,result) => {
    const {user_name, password} = params
    console.log('validate_access - Processor',params)
    const query = `
    CALL login_validate_access(?,?);`;
    mysqlConection.query(query, [user_name, password], (err, rows, fields) => {
        if(!err){
            // res.json();
            if(rows[0] === [] || rows[0].length === 0 ){
                result(null, ([]))
            }else{
                result(null, (JSON.stringify(rows[0])))
            }
        }else{
            console.log(err);
        }
    });
}; 