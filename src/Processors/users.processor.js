const mysqlConection = require('../Config/database')

exports.insertNewUser = (params,result) => {
    const {id, user_name, password, user_rol,email,phone,name,active} = params
    console.log('insertNewUser - processor',params)
    const query = `
    CALL insert_user(?,?,?,?,?,?,?,?);
    `;
    mysqlConection.query(query, 
        [id, 
        user_name, 
        password,
        user_rol,
        email,
        phone,
        name,
        active
    ], (err, rows, fields) => {
        if(!err){
            // res.json();
            result(null, (null,rows[0][0]))
        }else{
            console.log(err);
            result(err,'')
        }
    });
}; 

exports.getAllUsers = (result) => {
    const query = `CALL get_all_users`;
    mysqlConection.query(query, (err, rows, fields) => {
        console.log('err'.err)
        if(!err){
            console.log('processor: ok')           
            result(null,rows[0])
        }else{
            throw err
            result(err,'')
        }
    });   
}

exports.getUserById = (params,result) => {
    console.log('id:',params.id)
    const {id} = params;
    mysqlConection.query('SELECT * from users WHERE id = ?', [id], (err,
    rows, fields) =>{
        if(!err){
            result(null,rows)
        }else{
            console.log(err);
            result(err,'')
        }
    })
};   


exports.updateUserByID = (params,result) => {
    const {id, user_name, password, email, phone, active} = params
    const query = `
    CALL update_user(?,?,?,?,?,?);`;
    mysqlConection.query(query, [id, user_name, password, email, phone, active], (err, rows, fields) => {
        if(!err){
            result(null, ({status:'User Update'}))
        }else{
            console.log(err);
        }
    });
}; 


exports.deleteUserById = (params,result) => {
    console.log('id:',params.id)
    const {id} = params.id;
    mysqlConection.query('DELETE FROM users WHERE id = ?', [id], (err,
    rows, fields) =>{
        if(!err){
            result(null,{status:'User Delete'})
        }else{
            console.log(err);
        }
    })
};   


