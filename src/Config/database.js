const mysql = require('mysql');

const mysqlConection = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'1234',
    database: 'motor_company'
});

mysqlConection.connect(function (err){
    if(err){
        console.log(err);
        return;
    }else{
        console.log('DB is connected');
    }
});

module.exports = mysqlConection;



