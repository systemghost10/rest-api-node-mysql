CREATE DATABASE IF NOT EXISTS motor_company;

USE motor_company;

CREATE TABLE users(
id int NOT NULL auto_increment,
name varchar(45) NOT NULL,
password varchar(100) NOT NULL,
PRIMARY KEY(id)
);

DESCRIBE users